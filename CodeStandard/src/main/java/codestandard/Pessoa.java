package codestandard;

public class Pessoa {
    private int idadePessoa;
    private String nomePessoa;

    public String getNomePessoa() {
        return this.nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    } 

    public int getIdadePessoa() {
        return this.idadePessoa;
    }

    public void setIdadePessoa(int idadePessoa) {
        this.idadePessoa = idadePessoa;
    }
}
